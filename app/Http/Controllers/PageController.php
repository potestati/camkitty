<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;


class PageController extends Controller
{
    public $guzzle;

    function __construct() {
        $this->guzzle = new \GuzzleHttp\Client();
    }

    public function showHome(){
        $res = $this->guzzle->request('GET', 'https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json/?proplist=age',  [
            'headers' => [
                'Ocp-Apim-Subscription-Key' => '619b646e1e6a47c3b6aa4b7603cb2258'
            ]
        ]);

        $hosts =json_decode($res->getBody(), true);
        $data = $hosts['Response']['Data'];

        $hostsCount = 20;
        $roomId = 10;

        return view('welcome')->with('data', $data)->with('hostsCount', $hostsCount)->with('roomId', $roomId);
    }


    public function getHosts($roomId, $hostsCount){
        //dd('test');
      
        $res = $this->guzzle->request('GET', 'https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@top~' . $hostsCount . '@roomid~' . $roomId . '&proplist=age' ,  [
                  //https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json/?filter=freevc~0@bringvisitors~1@top~1000000@needtotal~1&proplist=age,roomid~54

            //https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@roomid~54&proplist=age

            'headers' => [
                'Ocp-Apim-Subscription-Key' => '619b646e1e6a47c3b6aa4b7603cb2258'
            ]
        ]);

        $hosts =json_decode($res->getBody(), true);
        $data = $hosts['Response']['Data'];
       
        // dd($data);    

        $hostsCount += 20;

        return view('welcome')->with('data', $data)->with('hostsCount', $hostsCount)->with('roomId', $roomId);
    }

        public function getSubcat($roomId, $subcat){
        //dd('test');

            $hostsCount = 20;
      
        $res = $this->guzzle->request('GET', 'https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@top~' . $hostsCount . '@roomid~' . $roomId . '&proplist=age&' . $subcat ,  [
                  //https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json/?filter=freevc~0@bringvisitors~1@top~1000000@needtotal~1&proplist=age,roomid~54

            //https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@roomid~54&proplist=age

            'headers' => [
                'Ocp-Apim-Subscription-Key' => '619b646e1e6a47c3b6aa4b7603cb2258'
            ]
        ]);

        $hosts =json_decode($res->getBody(), true);
        $data = $hosts['Response']['Data'];
       
        // dd($data);    

        $hostsCount += 20;

        return view('welcome')->with('data', $data)->with('hostsCount', $hostsCount)->with('roomId', $roomId);
    }

}

/*
https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@top~20@roomid~10&proplist=measurements&less=age~20

https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@top~20@roomid~10&proplist=age&less=age~30

https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@top~' . $hostsCount . '@roomid~' . $roomId . '&proplist=age&less=age~30

https://gstsvc.webcamwiz.com/imlapi_get_hostlist/v/2015-01-01/format/json?filter=freevc~0@top~20@roomid~' . $roomId . '&proplist=age&less=age~30

*/
