<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'PageController@showHome');

Route::get('category/{roomId}/hosts/{hostsCount}', 'PageController@getHosts')->name('getHosts');
// Route::get('category/{roomId}/load', 'PageController@load')->name('loadMore');
Route::get('category/{roomId}/subcategroy/{subcat}', 'PageController@getSubcat')->name('getSubcat');