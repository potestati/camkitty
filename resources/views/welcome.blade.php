<!DOCTYPE html>
<html>
    <head>
        <title>Hello Kitty</title>    
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="{{url('')}}/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('')}}/css/bootstrap-grid.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('')}}/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" type="text/css" href="{{url('')}}/css/main.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="{{url('')}}/js/bootstrap.bundle.min.js"></script>
        <script src="{{url('')}}/js/bootstrap.min.js"></script> 
        <script src="{{url('')}}/js/main.js"></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#"><img src="img/logo.png"/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">New</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Most Viewed</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Top Rated</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Premium</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Top Favorites</a>
                        </li>
                    </ul>
                    <div class="my-2 my-lg-0">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Join Cam Kitty for FREE</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <div class="main">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 host-search-bar">
                <div class="container">
                    <div clas="row">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hosts">
                            <a href="#">Hosts</a>
                            <a href="#">Become a host</a>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 search">
                            <form class="navbar-form navbar-left">
                                <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Search">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            {{--         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 tags-bar">
                <div class="row">
                    <a href="#">#asian</a>
                    <a href="#">#bigboobs</a>
                    <a href="#">#bbw</a>
                    <a href="#">#hairy</a>
                    <a href="#">#18</a>
                    <a href="#">#anal</a>
                    <a href="#">#squirt</a>
                    <a href="#">#ebony</a>
                    <a href="#">#milf</a>
                    <a href="#">#milk</a>
                    <a href="#">#mature</a>
                    <a href="#">#german</a>
                    <a href="#">#french</a>
                    <a href="#">#latina</a>
                    <a href="#">#pregnant</a>
                    <a href="#">#feet</a>
                    <a href="#">#new</a>
                    <a href="#">#teen</a>
                    <a href="#">#pantyhose</a>
                    <a href="#">#mistress</a>
                    <a href="#">#taboo</a>
                    <a href="#">#daddy</a>
                    <a href="#">#bigcock</a>
                    <a href="#">#young</a>
                    <a href="#">#slave</a>
                    <a href="#">#c2c</a>
                </div>
            </div> --}}

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cat-cam">
                <div class="categories">

                    <ul class="nav nav-tabs">
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 10, 'hostsCount' => 20])}}">Girl Alone</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 54, 'hostsCount' => 20])}}">Guy Alone Straight</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 160, 'hostsCount' => 20 ])}}">Shy Girl Alone</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 53, 'hostsCount' => 20])}}">Guy Alone Gay</a></li>

                        {{-- <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 11, 'hostsCount' => 20])}}">Girl Alone Lesbian</a></li> --}}
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 52, 'hostsCount' => 20])}}">Guy On Guy</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 191, 'hostsCount' => 20])}}">Girl On Girl</a></li>


                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 13, 'hostsCount' => 20])}}">Fetish BDSM</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 12, 'hostsCount' => 20 ])}}">Girl And Guy Couples</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 51, 'hostsCount' => 20])}}">Shemales</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 14, 'hostsCount' => 20])}}">Threesomes Groups</a></li>
                        <li class="cat-tab"><a href="{{route('getHosts', ['roomId' => 557,'hostsCount' => 20])}}">Shemale Couples</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="hot_girls" class="tab-pane fade in active show">
                            <table class="cat-table">
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'isany=fetish~5,7'])}}">Fetish</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'between=age~18,20'])}}">Between 18 and 20</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'between=age~20,30'])}}">Between 20 and 30</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'between=age~30,40'])}}">Between 30 and 40</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'greater=age~18'])}}">Greater than 18</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'less=age~30'])}}">Less than 30</a>
                                    </td>
                                    <td>532</td>
                                </tr>

                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'greaterorequal=age~18'])}}">Greater and equal 18</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'lessorequal=age~30'])}}">Less and equal 30</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="{{route('getSubcat', ['roomId' => $roomId, 'subcat' => 'exclude=age~30'])}}">Exclude 30</a>
                                    </td>
                                    <td>532</td>
                                </tr>
                            </table>
                        </div>
                        <div id="hot_bouys" class="tab-pane fade">
                            <table class="cat-table">
                                <tr>
                                    <td>Age 18-19</td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>Age 20-24</td>
                                    <td>532</td>
                                </tr>
                            </table>
                        </div>
                        <div id="group_sex" class="tab-pane fade">
                            <table class="cat-table">
                                <tr>
                                    <td>Age 18-19</td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>Age 20-24</td>
                                    <td>532</td>
                                </tr>
                            </table>
                        </div>
                        <div id="trans" class="tab-pane fade">
                            <table class="cat-table">
                                <tr>
                                    <td>Age 18-19</td>
                                    <td>532</td>
                                </tr>
                                <tr>
                                    <td>Age 20-24</td>
                                    <td>532</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="slide-button">CTEGORIES <i class="fas fa-angle-up"></i></div> 
                </div>

                <div class="row live-cams">
                    <h2 class="headline">Live cams</h2>

                    @foreach($data as $d)
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 home-cam">
                        <img src="{{$d['MainImg']}}" />
                        <div class="video-status live">Live Now</div>
                        <div class="video-info">
                            <span class="name">{{$d['NickName']}}</span>
                            <span class="age">{{$d['PropList']['age']}}</span>
                        </div>
                    </div>

                    @endforeach


                    <div class="row live-cams">
                        <h2 class="headline">Live cams</h2>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cam-button">
                            <a href="{{route('getHosts', ['roomId' => $roomId, 'hostsCount' => $hostsCount])}}">LOAD MORE</a>
                        </div>
                    </div>
                </div>
            </div>

            <footer>
                <div class="container">
                    <div class="row">
                        <ul class="footer-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Sitemap</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Customer Support Center</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Host Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Become a Host</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Affiliate Program</a>
                            </li>
                        </ul>

                        <ul class="footer-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Terms of Use</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Privacy Policy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Anty Spam Policy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Cookie Policy</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Refund Policy</a>
                            </li>
                        </ul>

                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>

                        <img src="{{url('')}}/img/footer-logos.png" />

                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </footer>
    </body>
</html>
